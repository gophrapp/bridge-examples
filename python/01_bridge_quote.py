import logging
import requests
import json

path_to_env_file = "../example.env"

with open(path_to_env_file, 'r') as lines:
    if not lines:
        logging.error('Cannot open py.env file.')
    
    env = {}
    for line in lines:
        line = line.strip()
        if not line or line.find('#') != -1:
            continue 

        key, value = line.split('=')
        key = key.strip()
        value = value.strip().strip('"')

        env.update({key: value})
        
body = {
    # All fields required unless noted
    # Typing only given if not a string
    'first_name': 'First',                
    'last_name': 'Last',                  
    'phone': '5555555555',     
    # optional, leave empty if not available           
    'email': 'email@example.com',         
    'address_1': '1000 Ryan St',  
    # optional, leave empty if not available
    'address_2': '',               
    'city': 'Lake Charles',               
    'state': 'LA',                        
    'zip': '70602',                       
    # just hard code this
    'country': 'US',                      
    # optional, leave empty if not available
    'pick_up_instructions': 'Instructions to us about how to pick up the item(s) at your location', 
    'drop_off_instructions': 'Instructions to us from the customer about how to drop things off',
    # do not provide unless the delivery date is in the future
    'scheduled_for': '2022-05-30',
    'items': [
        {
            # int
            'quantity': 1,      
            'name': 'Item 1',   
            # optional, leave empty if not available
            'sku': 'ABC-123',   
            # int
            'weight': 10  
        }
        # more items as necessary
    ]
}

headers = {
    'client-id': env['GOPHR_CLIENT_ID'],
    'client-secret': env['GOPHR_CLIENT_SECRET'],
    'Content-Type': 'application/json',
}

response = requests.post(
    url=f"{env['GOPHR_BRIDGE_HOST']}/quote",
    data=json.dumps(body),
    headers=headers
)

if response.status_code != 200:
    logging.error('Handle errors here')

# do stuff with the response
response_body = response.json()
print(response_body)
# Note response_body is a dict, but it's easier to pretty print json
# {
#   "status": "successful",
#   "status_code": 200,
#   "hasPayload": true,
#   "payload": {
#     "standard_quote": {
#       "quote_id": "f85c9e55-9063-4a22-9228-b8500c2f2e4e",
#       "fee": 17
#     },
#     "expedited_quote": {
#       "quote_id": "258376dc-b02e-4b9d-b58d-4a328528d595",
#       "fee": 22
#     }
#   }
# }