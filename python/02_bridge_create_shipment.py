import logging
import requests
import json

# This file should not be committed and the example that is committed here's values are fake
# Normally I would use pips dotenv library for messing with .env files, but I'm trying to 
# make this as system agnostic as possible. That said, I developed this on Linux so adjust
# the file path, or the entire environment variable handling, as necessary for your system.
path_to_env_file = "../example.env"
with open(path_to_env_file, 'r') as lines:
    if not lines:
        logging.error('Cannot open py.env file.')
    
    env = {}
    for line in lines:
        line = line.strip()
        if not line or line.find('#') != -1:
            continue 

        key, value = line.split('=')
        key = key.strip()
        value = value.strip().strip('"')

        env.update({key: value})

body = {
    # one of the two values from the quote 
    'quote_id': '47f47833-9145-4b41-9706-dfdcf8706651',
    # optional, leave empty if not available
    'drop_off_instructions': 'Instructions from the customer',
}

headers = {
    'client-id': env['GOPHR_CLIENT_ID'],
    'client-secret': env['GOPHR_CLIENT_SECRET'],
    'Content-Type': 'application/json',
}

response = requests.post(
    url=f"{env['GOPHR_BRIDGE_HOST']}/create",
    data=json.dumps(body),
    headers=headers
)

if response.status_code != 200:
    logging.error('Handle errors here')

# do stuff with the response
response_body = response.json()
print(response_body)

# Note response_body is a dict, but it's easier to pretty print json
# {
#     "status": "successful",
#     "status_code": 200,
#     "hasPayload": true,
#     "payload": {
#         "delivery_id": "1986",
#         "delivery_status": "created",
#         "scheduled_for": null,
#         "shipping_fee": 17,
#         "vehicle_type": "truck",
#         "is_expedited": false,
#         "distance": "0.4840",
#         "weight": "10.00",
#         "placed_at": "2021-06-25T16:38:12.994Z",
#         "items": [
#             "1-Item 1"
#         ],
#         "pick_up": {
#             "address_1": "619 Ryan Street",
#             "address_2": "Suite 205",
#             "city": " Lake Charles",
#             "state": "LA",
#             "zip": "70601",
#             "country": "US",
#             "lat": 30.2309,
#             "lng": -93.21645,
#             "instructions": "Instructions to us about how to pick up the item(s) at your location"
#         },
#         "drop_off": {
#             "address_1": "1000 Ryan St",
#             "address_2": "",
#             "city": "Lake Charles",
#             "state": "LA",
#             "zip": "70602",
#             "country": "US",
#             "lat": 30.22625,
#             "lng": -93.21838,
#             "instructions": "Instructions from the customer"
#         }
#     }
# }
