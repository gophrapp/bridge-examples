<?php
    // this uses the curl library (https://www.php.net/manual/en/book.curl)
    $curl = curl_init();

    // This file should not be committed and the example that is committed here's values are fake
    // Normally I would use a composer library for messing with .env files, but I'm trying to 
    // make this as system agnostic as possible. That said, I developed this on Linux so adjust
    // the file path, or the entire environment variable handling, as necessary for your system.
    $path_to_env_file = "../example.env";
    if (!is_readable($path_to_env_file)) {
        error_log("env file is not readable");
        exit(1);
    }

    $env = null;
    $lines = file($path_to_env_file, FILE_SKIP_EMPTY_LINES);
    foreach ($lines as $line) {
        // ignore comments
        if (strpos(trim($line), '#') === 0) {
            continue;
        }
        // separate key from value
        list($key, $value) = explode('=', $line, 2);
        $key = trim($key);
        $value = trim(trim($value), '"');

        $env[$key] = $value;
    }

    $body = array(
        // All fields required unless noted
        // Typing only given if not a string
        'first_name' => 'First',                
        'last_name' => 'Last',                  
        'phone' => '5555555555',     
        // optional, leave empty if not available           
        'email' => 'email@example.com',         
        'address_1' => '1000 Ryan St',  
        // optional, leave empty if not available
        'address_2' => '',               
        'city' => 'Lake Charles',               
        'state' => 'LA',                        
        'zip' => '70602',                       
        // just hard code this
        'country' => 'US',                      
        // optional, leave empty if not available
        'pick_up_instructions' => 'Instructions to us about how to pick up the item(s) at your location', 
        'drop_off_instructions' => 'Instructions to us from the customer about how to drop things off',
        # do not provide unless the delivery date is in the future
        'scheduled_for': '2022-05-30',
        'items' => array(
            array(
                // int
                'quantity' => 1,      
                'name' => 'Item 1',   
                // optional, leave empty if not available
                'sku' => 'ABC-123',   
                // int
                'weight' => 10,       
            ),
            // more items as necessary
        )
    );

    $headers = array(
        'client-id: ' . $env['GOPHR_CLIENT_ID'],
        'client-secret: ' . $env['GOPHR_CLIENT_SECRET'],
        'Content-Type: application/json'
    );

    $setopt = curl_setopt_array($curl, array(
        CURLOPT_URL => "{$env['GOPHR_BRIDGE_HOST']}/quote",
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS => json_encode($body),
        CURLOPT_HTTPHEADER => $headers,
        CURLOPT_RETURNTRANSFER => true,
    ));

    if (! $setopt) {
        error_log('Setopt broke');
    }

    $response = json_decode(curl_exec($curl));
    if (! $response) {
        error_log('No response from Gophr: ' . curl_errno($curl) . ': ' . curl_error($curl));
    } elseif ($response->status_code != 200) {
        error_log('Handle errors here');
    }

    // do stuff with the response
    print(json_encode($response, JSON_PRETTY_PRINT));
    // {
    //     "status": "successful",
    //     "status_code": 200,
    //     "hasPayload": true,
    //     "payload": {
    //         "standard_quote": {
    //             "quote_id": "e081a35f-130f-4fdf-be32-4fe5935a68d0",
    //             "fee": 17
    //         },
    //         "expedited_quote": {
    //             "quote_id": "4fba476d-c63f-40fc-9455-3452b28f7fe6",
    //             "fee": 22
    //         }
    //     }
    // }

    curl_close($curl);
?>