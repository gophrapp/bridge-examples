const axios = require('axios');

// This file should not be committed and the example that is committed here's values are fake
// Normally I would use a composer library for messing with .env files, but I'm trying to 
// make this as system agnostic as possible. That said, I developed this on Linux so adjust
// the file path, or the entire environment variable handling, as necessary for your system.
// I tried doing this in vanilla js but I hate node.
require('dotenv').config({ path: '../example.env' });

const body = {
    // one of the two values from the quote 
    'quote_id': '508f12b3-b277-4419-8bec-b5ca7c903789',
    // optional, leave empty if not available
    'drop_off_instructions': 'Instructions from the customer',
};

const headers = {
    'client-id': process.env.GOPHR_CLIENT_ID,
    'client-secret': process.env.GOPHR_CLIENT_SECRET,
    'Content-Type': 'application/json',
    'Content-Length': body.length
};

axios
    .post(`${ process.env.GOPHR_BRIDGE_HOST }/create`, body, {
        headers: headers
    })
    .then((res) => {
        // Do stuff with the response
        console.log(res.data);
        // {
        //     "status": "successful",
        //     "status_code": 200,
        //     "hasPayload": true,
        //     "payload": {
        //         "delivery_id": "1986",
        //         "delivery_status": "created",
        //         "scheduled_for": null,
        //         "shipping_fee": 17,
        //         "vehicle_type": "truck",
        //         "is_expedited": false,
        //         "distance": "0.4840",
        //         "weight": "10.00",
        //         "placed_at": "2021-06-25T16:38:12.994Z",
        //         "items": [
        //             "1-Item 1"
        //         ],
        //         "pick_up": {
        //             "address_1": "619 Ryan Street",
        //             "address_2": "Suite 205",
        //             "city": " Lake Charles",
        //             "state": "LA",
        //             "zip": "70601",
        //             "country": "US",
        //             "lat": 30.2309,
        //             "lng": -93.21645,
        //             "instructions": "Instructions to us about how to pick up the item(s) at your location"
        //         },
        //         "drop_off": {
        //             "address_1": "1000 Ryan St",
        //             "address_2": "",
        //             "city": "Lake Charles",
        //             "state": "LA",
        //             "zip": "70602",
        //             "country": "US",
        //             "lat": 30.22625,
        //             "lng": -93.21838,
        //             "instructions": "Instructions from the customer"
        //         }
        //     }
        // }
    })
    .catch((error) => {
        // handle errors
        console.error(error.data);
    });
