<?php
    // this uses the curl library (https://www.php.net/manual/en/book.curl)
    $curl = curl_init();

    // This file should not be committed and the example that is committed here's values are fake
    // Normally I would use a composer library for messing with .env files, but I'm trying to 
    // make this as system agnostic as possible. That said, I developed this on Linux so adjust
    // the file path, or the entire environment variable handling, as necessary for your system.
    $path_to_env_file = "../example.env";
    if (!is_readable($path_to_env_file)) {
        error_log("env file is not readable");
        exit(1);
    }

    $env = null;
    $lines = file($path_to_env_file, FILE_SKIP_EMPTY_LINES);
    foreach ($lines as $line) {
        // ignore comments
        if (strpos(trim($line), '#') === 0) {
            continue;
        }
        // separate key from value
        list($key, $value) = explode('=', $line, 2);
        $key = trim($key);
        $value = trim(trim($value), '"');

        $env[$key] = $value;
    }

    $body = array(
        // one of the two values from the quote 
        'quote_id' => 'e081a35f-130f-4fdf-be32-4fe5935a68d0',
        // optional, leave empty if not available
        'drop_off_instructions' => 'Instructions from the customer',
    );

    $headers = array(
        'client-id: ' . $env['GOPHR_CLIENT_ID'],
        'client-secret: ' . $env['GOPHR_CLIENT_SECRET'],
        'Content-Type: application/json'
    );

    $setopt = curl_setopt_array($curl, array(
        CURLOPT_URL => "{$env['GOPHR_BRIDGE_HOST']}/create",
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS => json_encode($body),
        CURLOPT_HTTPHEADER => $headers,
        CURLOPT_RETURNTRANSFER => true,
    ));

    if (! $setopt) {
        error_log('Setopt broke');
    }

    $response = json_decode(curl_exec($curl));
    if (! $response) {
        error_log('No response from Gophr: ' . curl_errno($curl) . ': ' . curl_error($curl));
    } elseif ($response->status_code != 200) {
        error_log('Handle errors here');
    }

    // Do stuff with the response
    print(json_encode($response, JSON_PRETTY_PRINT));
    // {
    //     "status": "successful",
    //     "status_code": 200,
    //     "hasPayload": true,
    //     "payload": {
    //         "delivery_id": "1986",
    //         "delivery_status": "created",
    //         "scheduled_for": null,
    //         "shipping_fee": 17,
    //         "vehicle_type": "truck",
    //         "is_expedited": false,
    //         "distance": "0.4840",
    //         "weight": "10.00",
    //         "placed_at": "2021-06-25T16:38:12.994Z",
    //         "items": [
    //             "1-Item 1"
    //         ],
    //         "pick_up": {
    //             "address_1": "619 Ryan Street",
    //             "address_2": "Suite 205",
    //             "city": " Lake Charles",
    //             "state": "LA",
    //             "zip": "70601",
    //             "country": "US",
    //             "lat": 30.2309,
    //             "lng": -93.21645,
    //             "instructions": "Instructions to us about how to pick up the item(s) at your location"
    //         },
    //         "drop_off": {
    //             "address_1": "1000 Ryan St",
    //             "address_2": "",
    //             "city": "Lake Charles",
    //             "state": "LA",
    //             "zip": "70602",
    //             "country": "US",
    //             "lat": 30.22625,
    //             "lng": -93.21838,
    //             "instructions": "Instructions from the customer"
    //         }
    //     }
    // }

    curl_close($curl);
?>