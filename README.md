# README #

This is an example implementation in Python, PHP and NodeJS for [Gophr's](https://www.gophrapp.com/) "Bridge" API for merchants with an existing digital marketplace.

## Use ##

This is simple a guide to how Bridge could be implemented. The general idea here is you send off a request for a quote, display the quotes to your user for selection, and use said selection to create a delivery request in Gophr's system.

- You must to be signed up as a [Merchant Partner](https://share.hsforms.com/1C2L91k2dSV23YcM77XDzEw3k6zf) with Gophr.
- Your client id and secret can then be obtained via our internal tool dashboard.
- Integrate the two RESTful API calls into your digital marketplace.
- Once an order comes in from your existing marketplace someone in the physical store will have to pick the items.
- Then someone in the store will mark the shipment as ready for a Gophr driver to facilitate the delivery in the internal tool dashboard.

## Dependencies ##

### [Python](https://www.python.org/) ###
While developed in Python 3.8 2.x should work as well. There are no external package dependencies for this implementation.

### [PHP](https://www.php.net/) ###
Developed in PHP 7.4.3 and requires no external package dependencies.

### [NodeJS](https://nodejs.org) ###
Developed in NodeJS v 10.19.0 with NPM 6.14.4. This implementation is dependent upon the [Axios](https://www.npmjs.com/package/axios) and [dotenv](https://www.npmjs.com/package/dotenv) libraries.